SELECT  CheckGoodsID ,
        ColorID ,
        CAST(Number AS INT) AS Number ,
        Size
FROM    dbo.CheckDetail UNPIVOT( Number FOR SIZE IN ( s1, s2, s3, s4, s5, s6,
                                                      s7, s8, s9, s10, s11,
                                                      s12, s13, s14, s15, s16,
                                                      s17 ) ) AS OUP
WHERE   number <> 0
        AND checkgoodsid IN ( 'KCDN003919002', 'KNDP000028001' )
