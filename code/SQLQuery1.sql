DECLARE @startdate DATETIME ,
    @enddate DATETIME ,
    @Group NVARCHAR(100) ,
    @JIN NVARCHAR(4) ,
    @QU NVARCHAR(4) 
    
SET @startdate = '2010-1-1'
SET @enddate = '2010-7-31'
SET @group = 'AD'
    
SET @JIN = CAST(YEAR(@enddate) AS NVARCHAR(4))
SET @qu = CAST(YEAR(DATEADD("Year", -1, @enddate)) AS NVARCHAR(4))
    
DECLARE @TB TABLE
    (
      Region NVARCHAR(20) ,
      [Year] NVARCHAR(4) ,
      Total NUMERIC(18, 4)
    )
    
INSERT  INTO @TB
        SELECT  dbo.Customer.Region ,
                dbo.Goods.[Year] ,
                dbo.Goods.UnitPrice * dbo.DeliveryGoods.Quantity
        FROM    dbo.DeliveryGoods
                INNER JOIN ( SELECT DeliveryID ,
                                    Warehouse_No ,
                                    Customer_id
                             FROM   dbo.Delivery
                             WHERE  Delivery_Date BETWEEN @startdate AND @enddate
                                    AND [Type] = 1
                                    AND Customer_id IN (
                                    SELECT  Customer_id
                                    FROM    dbo.Customer
                                    WHERE   Mathod = '����'
                                            AND [Group] = @Group )
                           ) AS A ON dbo.DeliveryGoods.DeliveryID = A.DeliveryID
                INNER JOIN dbo.Goods ON dbo.Goods.Goods_no = dbo.DeliveryGoods.Goods_No
                LEFT JOIN dbo.Customer ON A.Customer_id = dbo.Customer.Customer_id

        
        
INSERT  INTO @TB
        SELECT  dbo.Customer.Region ,
                dbo.Goods.[Year] ,
                dbo.Goods.UnitPrice * dbo.PuReceiptGoods.Quantity * -1
        FROM    dbo.PuReceiptGoods
                INNER JOIN ( SELECT PureceiptID ,
                                    Customer_ID
                             FROM   dbo.PuReceipt
                             WHERE  Receipt_Date BETWEEN @startdate AND @enddate
                                    AND TYPE = 3
                                    AND Customer_id IN (
                                    SELECT  Customer_id
                                    FROM    dbo.Customer
                                    WHERE   Mathod = '����'
                                            AND [Group] = @Group )
                           ) AS A ON dbo.PuReceiptGoods.PureceiptID = A.PureceiptID
                INNER JOIN dbo.Goods ON dbo.PuReceiptGoods.Goods_No = dbo.Goods.Goods_no
                LEFT JOIN dbo.Customer ON A.Customer_ID = dbo.Customer.Customer_id


SELECT  Region ,
        ToTal = SUM(Total) ,
        [JIN] = SUM(CASE WHEN [year] = @jin THEN Total
                         ELSE 0
                    END) ,
        [QU] = SUM(CASE WHEN [year] = @qu THEN Total
                        ELSE 0
                   END) ,
        [QIAN] = SUM(CASE WHEN [year] NOT IN ( @jin, @qu ) THEN Total
                          ELSE 0
                     END)
FROM    @TB
GROUP BY Region

        