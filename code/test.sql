DECLARE @JIN NVARCHAR(4) ,
    @QU NVARCHAR(4) 
DECLARE @StartDate DATETIME ,
    @Enddate DATETIME 
SET @StartDate = '2011-1-1'
SET @Enddate = '2011-1-31'
DECLARE @group NVARCHAR(10)
SET @group = 'AD'

SET @JIN = CAST(YEAR(@ENDDATE) AS NVARCHAR(4))
SET @QU = CAST(YEAR(DATEADD("YEAR", -1, @ENDDATE)) AS NVARCHAR(4))
    
DECLARE @TB TABLE
    (
      REGION NVARCHAR(20) ,
      [YEAR] NVARCHAR(4) ,
      TOTAL NUMERIC(18, 4)
    )
    
INSERT  INTO @TB
        SELECT  DBO.CUSTOMER.REGION ,
                DBO.GOODS.[YEAR] ,
                DBO.GOODS.UNITPRICE * DBO.DELIVERYGOODS.QUANTITY
        FROM    DBO.DELIVERYGOODS
                INNER JOIN ( SELECT DELIVERYID ,
                                    WAREHOUSE_NO ,
                                    CUSTOMER_ID
                             FROM   DBO.DELIVERY
                             WHERE  DELIVERY_DATE BETWEEN @STARTDATE AND @ENDDATE
                                    AND customer_id IN (
                                    SELECT  CUSTOMER_ID
                                    FROM    DBO.CUSTOMER
                                    WHERE   MATHOD = '����'
                                            AND [group] = @group )
                           ) AS A ON DBO.DELIVERYGOODS.DELIVERYID = A.DELIVERYID
                INNER JOIN DBO.GOODS ON DBO.GOODS.GOODS_NO = DBO.DELIVERYGOODS.GOODS_NO
                LEFT JOIN DBO.CUSTOMER ON A.CUSTOMER_ID = DBO.CUSTOMER.CUSTOMER_ID

        
        
INSERT  INTO @TB
        SELECT  DBO.CUSTOMER.REGION ,
                DBO.GOODS.[YEAR] ,
                DBO.GOODS.UNITPRICE * DBO.PURECEIPTGOODS.QUANTITY * -1
        FROM    DBO.PURECEIPTGOODS
                INNER JOIN ( SELECT PURECEIPTID ,
                                    CUSTOMER_ID
                             FROM   DBO.PURECEIPT
                             WHERE  RECEIPT_DATE BETWEEN @STARTDATE AND @ENDDATE
                                    AND CUSTOMER_ID IN (
                                    SELECT  CUSTOMER_ID
                                    FROM    DBO.CUSTOMER
                                    WHERE   MATHOD = '����'
                                            AND [group] = @group )
                           ) AS A ON DBO.PURECEIPTGOODS.PURECEIPTID = A.PURECEIPTID
                INNER JOIN DBO.GOODS ON DBO.PURECEIPTGOODS.GOODS_NO = DBO.GOODS.GOODS_NO
                LEFT JOIN DBO.CUSTOMER ON A.CUSTOMER_ID = DBO.CUSTOMER.CUSTOMER_ID


SELECT  Region ,
        ToTal = SUM(Total) ,
        [JIN] = SUM(CASE WHEN [year] = @JIN THEN Total
                         ELSE 0
                    END) ,
        [QU] = SUM(CASE WHEN [year] = @QU THEN Total
                        ELSE 0
                   END) ,
        [QIAN] = SUM(CASE WHEN [year] NOT IN ( @JIN, @QU ) THEN Total
                          ELSE 0
                     END)
FROM    @TB
GROUP BY Region