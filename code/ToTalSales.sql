DECLARE @StartDate DATETIME ,
    @EndDate DATETIME
SET @StartDate = '2010-5-1'
SET @EndDate = '2010-6-1'


SELECT  A.* ,
        B.LastSales ,
        B.LastSales ,
        RaiseRate = CASE WHEN LastSales IS NULL THEN NULL
                         ELSE ( A.Sales - B.LastSales ) / B.LastSales
                    END
FROM    ( SELECT    dbo.[Check].CheckDate ,
                    Sales = SUM(dbo.CheckGoods.Quantity
                                * dbo.CheckGoods.DiscountPrice) ,
                    Label = SUM(dbo.CheckGoods.Quantity * goods.UnitPrice) ,
                    dbo.Customer.[Group]
          FROM      dbo.[Check]
                    INNER JOIN dbo.CheckGoods ON dbo.[Check].CheckID = dbo.CheckGoods.CheckID
                    INNER JOIN dbo.Goods ON dbo.CheckGoods.Goods_No = dbo.Goods.Goods_no
                    INNER JOIN dbo.Customer ON dbo.[Check].Customer_ID = dbo.Customer.Customer_id
          WHERE     ( dbo.[Check].CheckDate BETWEEN @StartDate
                                            AND     @EndDate )
          GROUP BY  dbo.Customer.[Group] ,
                    dbo.[Check].CheckDate
        ) AS A
        FULL JOIN ( SELECT  dbo.[Check].CheckDate ,
                            LastSales = SUM(dbo.CheckGoods.Quantity
                                            * dbo.CheckGoods.DiscountPrice) ,
                            LastLabel = SUM(dbo.CheckGoods.Quantity
                                            * goods.UnitPrice) ,
                            dbo.Customer.[Group]
                    FROM    dbo.[Check]
                            INNER JOIN dbo.CheckGoods ON dbo.[Check].CheckID = dbo.CheckGoods.CheckID
                            INNER JOIN dbo.Goods ON dbo.CheckGoods.Goods_No = dbo.Goods.Goods_no
                            INNER JOIN dbo.Customer ON dbo.[Check].Customer_ID = dbo.Customer.Customer_id
                    WHERE   ( dbo.[Check].CheckDate BETWEEN DATEADD("Year", -1,
                                                              @StartDate)
                                                    AND     DATEADD("Year", -1,
                                                              @EndDate) )
                    GROUP BY dbo.Customer.[Group] ,
                            dbo.[Check].CheckDate
                  ) AS B ON A.[Group] = B.[Group]
                            AND A.CheckDate = DATEADD("Year", 1, B.CheckDate)
       

