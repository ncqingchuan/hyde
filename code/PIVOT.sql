DECLARE @I INT ,
    @COUNT INT ,
    @WHERE NVARCHAR(1000)
SET @I = 1
SET @WHERE = ''
SELECT  @COUNT = MAX(A.CNT)
FROM    ( SELECT    COUNT(FIELDNAME) AS CNT
          FROM      DICTSIZE
          WHERE     POSTED = 1
          GROUP BY  SIZE_CLASS
        ) AS A

WHILE @I <= @COUNT 
    BEGIN
        SET @WHERE = @WHERE + 'S' + CAST(@I AS NVARCHAR(10)) + ','
        SET @I = @I + 1
    END

SET @WHERE = LEFT(@WHERE, LEN(@WHERE) - 1)

EXEC('SELECT  *
FROM    ( SELECT    size_class ,
SIZE ,
fieldname
FROM      dbo.DictSize
) AS DS PIVOT( MAX(size) FOR fieldname IN ( '+@WHERE+' ) ) AS P')
                                                    
                                                    